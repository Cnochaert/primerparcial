#Primer parcial / Cnochaert Andrés .

#Comentarios. Profe le marco lo que no entendi 
#en caso de los carnívoros alertar en caso de pasar una planta 
#!!Carnivoro y Hervivoro son una especificación de Animal una planta  no pasa nunca x ahi, hacia arriba tiene Vegetal 
#y lo mismo para los herbívoros en caso de recibir un animal 
#!! Todos los que pasan x hervivoros son animales.  


#Creo dos Listas 
listaanimales = list()
listavegetales= list()

#Creo las Clases
class Animal:
    def __init__(self,tipo):
        self.tipo = tipo
        
    # Método genérico pero con implementación particular
    def comer(self):
        pass

class Vegetacion:
    def __init__(self,tipo):
        self.tipo = tipo
        
    # Método genérico pero con implementación particular
    def comer(self):
        pass

## ! Clasificación Vegetación
class Pasto(Vegetacion):
    def comer(self):
        print("Soy un ", type(self).__name__, " y soy ", self.tipo)

class Flor(Vegetacion):
    def comer(self):
        print("Soy una ", type(self).__name__, " y soy ", self.tipo)

class Planta(Vegetacion):
    def comer(self):
        print("Soy una ", type(self).__name__, " y soy ", self.tipo)


## ! Clasificación Animales 
class Carnivoro(Animal):
    def __init__(self,tipo):
        super().__init__(tipo)

class Hervivoro(Animal):
    def __init__(self,tipo):
        super().__init__(tipo)

class Tigre(Carnivoro):
    def comer(self):
        print("Soy un ", type(self).__name__, " y soy ", self.tipo)

class Leon(Carnivoro):
   def comer(self):
        print("Soy un ", type(self).__name__, " y soy ", self.tipo)

class Ciervo(Hervivoro):
    def comer(self):
        print("Soy un ", type(self).__name__, " y soy ", self.tipo)

class Cebra(Hervivoro):
   def comer(self):
        print("Soy una ", type(self).__name__, " y soy ", self.tipo)

##! Instancio Animales y cargo Lista 
mi_tigre = Tigre('Carnívoro')
listaanimales.append(mi_tigre)

mi_leon = Leon('Carnívoro')
listaanimales.append(mi_leon)

mi_ciervo = Ciervo('Hervivoro')
listaanimales.append(mi_ciervo)

mi_cebra = Cebra('Hervivoro')
listaanimales.append(mi_cebra)

##! Instancio Vegetales y cargo Lista 
mi_pasto = Pasto('Vegetacion')
listavegetales.append(mi_pasto)

mi_flor = Flor('Vegetacion')
listavegetales.append(mi_flor)

mi_planta = Planta('Vegetacion')
listavegetales.append(mi_planta)


##! Recorro Lista
for a in listaanimales:
    a.comer()

for v in listavegetales:
    v.comer()